// console.log("Hi")

// WHILE LOOP

/*	
let count = 5

while(count !== 0){
	console.log("while: " + count);

	// decrement
	count--;
	console.log("Value of count after the Iteration: " + count)

}
*/

// DO WHILE LOOP

	/* syntax
		do:{
			statement;
			iteration;
		}
		while(condition)
	*/
/*
	let number = Number(prompt("Give me a number."));

	do{
		console.log("Do while: " + number);

		number++;
	}while(number < 10);
*/


// FOR LOOP 
	/* Syntax:
		for(initialization; condition; finalExpession){
			statement/s;
		}
	*/
/*
	// create a loop that will start from 0 and end at 20
	for(let count=0; count<=20; count++){
		console.log("The current value of count is " +count);
	}
*/

	// cout=nting of strings
	let myString = "Meracle";
		console.log(myString.length);
		// console.log(myString[0]);
		// console.log(myString[1]);
		// console.log(myString[2]);
		// console.log(myString[3]);

	/*	
		for(let i=0; i < myString.length; i+=2){
			console.log(myString[i]);
		}

		let numA = 15;

		for(let i=0; i<=10; i++){
			let exponential = numA**i
			
			console.log(exponential);
		}
		*/

// Create a string named "myName" with value of "Alex"
	// print out number 3 when the letter is vowel
		let myName = "Meracle";

	for (let i=0; i < myName.length; i++){
		if (myName[i] .toLowerCase() === "a" ||
			myName[i] .toLowerCase() === "e" ||
			myName[i] .toLowerCase() === "i" ||
			myName[i] .toLowerCase() === "o" ||
			myName[i] .toLowerCase() === "u"){

			console.log(3);
			}
			else{
				console.log(myName[i]);
			}
			
	}

// CONTINUE & BREAK STATEMENTS

	for(let count=0; count<=20; count++){
		// if remainder is equal to 0
			if (count % 2 == 0){
				continue;
				
			}
			else if(count >10 ){
				break;  
			}
			console.log("continue and break: " + count);
		}
	
	// Another example

		let name = "alexandro"

		for(let i=0; i < name.length; i++ ){
			if (name[i] .toLowerCase() === "a"){
				console.log("continue to next iteration")
				continue;
			}
			if(name[i] === "d"){
				console.log("Console log before the break!")
				break;
			}
			console.log(name[i])
		}

